import { useSelector } from "react-redux";
import Product from '../product';

const Products = () => {
    const products = useSelector( state => state.products)
    return(
        <div style={{columnCount:'3'}}>
            {
                products.map((product, index) => <Product key={index} product={ product }/> )
            }
        </div>
    )
}
export default Products;