import { useDispatch } from 'react-redux';
import { addToCartThunk, removeFromCartThunk } from '../../store/modules/cart/thunks';

const Product = ({product, isRemovable = false}) => {

    const dispatch = useDispatch();
    return (<div>
        <div>{product.name} | <span>{product.type} </span></div>

        {isRemovable ? 
        <button onClick={() => dispatch(removeFromCartThunk(product.id))}>Remover do Carrinho</button>
        :
        <button onClick={() => dispatch(addToCartThunk(product))}>Adicionar ao Carrinho</button>
        }
    </div>)
}
export default Product;